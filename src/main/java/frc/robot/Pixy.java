package frc.robot;

import java.util.ArrayList;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import java.lang.Math;

import io.github.pseudoresonance.pixy2api.Pixy2;
import io.github.pseudoresonance.pixy2api.links.SPILink;
import io.github.pseudoresonance.pixy2api.Pixy2CCC.Block;

public class Pixy {

	private static Pixy2 pixy;

    private static double angle_threshold = 1;
    private static double angle_offset = 15;
	private static double aim_min = 0.39; // 0.39
	private static double drive_min = 0.5; //0.42
    private static double aimKp = 0.035;
	private static double DEGREE_RANGE = 30.26;
	private double aim_adjust;
	private double drive_adjust;
	
	private Block targetBlock;
	private ArrayList<Block> blocks;

    public Pixy() {
		
    }

    public void initialize() {
        // System.out.println("Initializing Pixy SPI Link");

        pixy = Pixy2.createInstance(new SPILink()); // Creates a new Pixy2 camera using SPILink
		pixy.init(); // Initializes the camera and prepares to send/receive data

		// System.out.println("Initialized Pixy SPI Link");
    }
    
    public Block getLargestBlock(byte signature) {
        // Original example code from https://github.com/PseudoResonance/Pixy2JavaAPI/wiki/Using-the-API#finding-the-biggest-target-in-the-frame

        // Gets the number of "blocks", identified targets, that match signature 1 on the Pixy2,
		// does not wait for new data if none is available,
		// and limits the number of returned blocks to 25, for a slight increase in efficiency
		int blockCount = pixy.getCCC().getBlocks(false, signature, 25);
		// System.out.println("Found " + blockCount + " blocks!"); // Reports number of blocks found
		if (blockCount <= 0) {
			return null; // If blocks were not found, stop processing
		}
		ArrayList<Block> blocks = pixy.getCCC().getBlockCache(); // Gets a list of all blocks found by the Pixy2
		Block largestBlock = null;
		for (Block block : blocks) { // Loops through all blocks and finds the widest one
			if (largestBlock == null) {
				largestBlock = block;
			} else if ((block.getWidth() > largestBlock.getWidth())) {
				largestBlock = block;
			}
		}
		return largestBlock;
	}

	public double getTargetAngle(Block object){ // Returns the angle relative to Pixy's center
		
		if (object == null){
			return -500;
		}

		// double fov_angle = 60.51;
		double half_fov_pixels = 157.5;
		double object_pixels = 0;
		double angle = 0;
		int raw_x_pixel = object.getX();

		object_pixels = raw_x_pixel-half_fov_pixels;
		angle = Math.toDegrees(Math.atan(object_pixels/270.016)); 

		return angle;
	}

    public boolean seek(byte signature) {
		Block ballBlock = getLargestBlock(signature);
		// Block ballBlock = getTargetBlock();
		double angle = getTargetAngle(ballBlock);
		SmartDashboard.putNumber("Pixy Angle", angle);
		System.out.print("Pixy Angle: " + angle);

        aim_adjust = aimKp * (angle / DEGREE_RANGE);
        if (angle < -400) {
			aim_adjust = 0.39;
		} else if (angle - angle_offset > angle_threshold) {
            aim_adjust += aim_min;
        } else if (angle - angle_offset < -angle_threshold) {
            aim_adjust -= aim_min;
        } else {
            aim_adjust = 0;
			Robot.drive.limelightDrive(0, 0);
			setTargetBlock(ballBlock);
            return true;
        }

        Robot.drive.limelightDrive(0, aim_adjust);

        return false;
    }

	public boolean pixyAutoDrive() {
		Block ballBlock = getTargetBlock();
		double angle = getTargetAngle(ballBlock);
		
		if (angle > -400) {
			Robot.drive.limelightDrive(0.39, 0.0);
		} else {
			Robot.drive.limelightDrive(0.0, 0.0);
			return true;
		}

		return false;
	}

	public boolean aquire() {
		updateTargetBlock();

		Block ballBlock = getTargetBlock();
		double angle = getTargetAngle(ballBlock);
		// SmartDashboard.putNumber("Pixy Angle", angle);
		// System.out.print("Pixy Angle: " + angle);

		aim_adjust = aimKp * (angle / DEGREE_RANGE);
		drive_adjust = 0;

		if (angle < -400) {
			Robot.drive.limelightDrive(0.0, 0.0);
			System.out.println("BALL AQUIRED!!!");
			return true;
		} else if (angle - angle_offset > angle_threshold) {
			drive_adjust += drive_min;
			aim_adjust += aim_min;
			System.out.println("Aim Adjust: " + aim_adjust);
			System.out.println("Drive Adjust: " + drive_adjust);
        } else if (angle - angle_offset < -angle_threshold) {
			drive_adjust += drive_min;
			aim_adjust -= aim_min;
			System.out.println("Aim Adjust: " + aim_adjust);
			System.out.println("Drive Adjust: " + drive_adjust);
		} else {
			aim_adjust = 0;
			drive_adjust += drive_min;
			System.out.println("AIMED");
			// System.out.println("Drive Adjust: " + drive_adjust);
        }

        Robot.drive.limelightDrive(drive_adjust, aim_adjust);

        return false;
	}

	public void updateBlockCache() {
		blocks = pixy.getCCC().getBlockCache(); // Gets a list of all blocks found by the Pixy2
		// System.out.println("UPDATED BLOCK CACHE " + blocks);
	}

	public void updateTargetBlock() {
		int signature = targetBlock.getSignature();
		int blockCount = pixy.getCCC().getBlocks(false, signature, 25);
		boolean found = false;

		System.out.println("UPDATE: Found " + blockCount + " blocks!"); // Reports number of blocks found
		// if (blockCount <= 0) {
		// 	targetBlock = null; // If blocks were not found, stop processing
		// }

		if (targetBlock == null) {

		}

		updateBlockCache();

		for (Block block : blocks) { // Loops through all the new blocks and finds the one that matches the old one's index
			if (block.getIndex() == targetBlock.getIndex()) {
				targetBlock = block;
				System.out.println("NEW BLOCK WAS FOUND. Old: " + targetBlock.getX() + " New: " + block.getX());
				found = true;
			}
		}
		if (!found) {  // If no new block was found that matches the old block (in index), return null
			targetBlock = null;
			System.out.println("NEW TARGET BLOCK NOT FOUND: " + targetBlock);
		}
		// System.out.println("Block Update EXIT");
	}
	
	public void setTargetBlock(Block block) {
		targetBlock = block;
		// System.out.println("Set Target Block");
		// targetBlock = getLargestBlock(Pixy2CCC.CCC_SIG1);
	}

	public Block getTargetBlock() {
		// int xpos;
		// if (targetBlock == null) {
		// 	xpos = -1;
		// } else {
		// 	xpos = targetBlock.getX();
		// }
		try {
			System.out.println("Got Target Block with INDEX: " + targetBlock.getIndex() + " and with X POS: " + targetBlock.getX());
		} catch (Exception e) {
			System.out.println("Got NO Target Block");
		}
		
		return targetBlock;
	}
    
}