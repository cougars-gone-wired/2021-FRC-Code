package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class StopIntaking extends CommandBase{
    @Override
    public void initialize() {
        Robot.chomper.setShooterReady();
        Robot.intake.setNotMoving();
        Robot.feeder.setNotMoving();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
    
