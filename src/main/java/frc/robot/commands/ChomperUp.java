package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class ChomperUp extends CommandBase {

    @Override
    public void initialize() {
        Robot.chomper.setIdle();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}