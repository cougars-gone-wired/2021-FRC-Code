package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class ShootVoltage extends CommandBase {

    private double shooterSpeed;

    public ShootVoltage() {
        shooterSpeed = Robot.shooter.getShooterSpeed();
        Robot.shooter.setShooterSpeed(shooterSpeed);
    }

    public ShootVoltage(double shooterSpeed, boolean changeThresholds) {
        if(changeThresholds) {
            Robot.shooter.setVelocityThresholds(100, 100);
        }
        this.shooterSpeed = shooterSpeed;
        Robot.shooter.setShooterSpeed(shooterSpeed);
    }

    @Override
    public void execute() {
        Robot.chomper.autoChomp();
        Robot.feeder.controlAutoShooterFeed();
        Robot.shooter.setShooterSpeed(shooterSpeed);
        Robot.shooter.setVoltageShooting();
    }

    @Override
    public void end(boolean interrupted) {
        Robot.shooter.setNotMoving();
        Robot.shooter.setShooter(.25);
    }
}