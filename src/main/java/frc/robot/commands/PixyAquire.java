package frc.robot.commands;


import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class PixyAquire extends CommandBase {
    boolean isAte;

    @Override
    public void execute() {
        isAte = Robot.pixy.aquire();
    }

    @Override
    public boolean isFinished() {
        return isAte;
        // return false;
    }
}
