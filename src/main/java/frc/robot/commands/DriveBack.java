package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.Constants.DriveConstants;

public class DriveBack extends CommandBase {

    private double distance;
    private double speed;

    public DriveBack(double distance) {
        this.distance = distance;
        this.speed = DriveConstants.AUTO_DRIVE_SPEED;
    }

    public DriveBack(double distance, double speed) {
        this.distance = distance;
        this.speed = speed;
    }

    @Override
    public void execute() {
        Robot.drive.driveStraight(-speed);
    }

    @Override
    public boolean isFinished() {
        if (Robot.drive.getEncoders().getAvgEncoderMetersAvg() <= -distance) {
            Robot.drive.driveStraight(0);
            Robot.drive.getEncoders().resetEncoders();
            return true;
        }
        return false;
    }
}