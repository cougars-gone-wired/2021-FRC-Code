package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.Shooter;

public class ShootPID extends CommandBase {

    private double desiredVelocity;

    public ShootPID() {
        desiredVelocity = Shooter.INITIAL_DESIRED_VELOCITY;
        Robot.shooter.setDesiredVelocity(desiredVelocity);
    }

    public ShootPID(double desiredVelocityPercent) {
        desiredVelocity = desiredVelocityPercent * Shooter.VOLTAGE_TO_VELOCITY;
        Robot.shooter.setDesiredVelocity(desiredVelocity);
    }

    @Override
    public void execute() {
        Robot.chomper.autoChomp();
        Robot.feeder.controlAutoShooterFeed();
        Robot.shooter.setPIDShooting();
    }

    @Override
    public void end(boolean interrupted) {
        Robot.shooter.setNotMoving();
    }

}