package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.ArmDown;
import frc.robot.commands.ArmUp;
import frc.robot.commands.ChomperDown;
import frc.robot.commands.ChomperUp;
import frc.robot.commands.DoNothing;
import frc.robot.commands.IntakeArmDown;
import frc.robot.commands.IntakeArmUp;

//Called Perpetually in AutoSelector!
public class PistonTest extends SequentialCommandGroup {
    private int delay = 30;

    public PistonTest() {
        addCommands(
            new ParallelCommandGroup(
                new DoNothing(delay),
                new ArmUp(),
                new IntakeArmUp(),
                new ChomperUp()
                ),
            new ParallelCommandGroup(
                new DoNothing(delay),
                new ArmDown(),
                new IntakeArmDown(),
                new ChomperDown()
                )
        );
    }
}
