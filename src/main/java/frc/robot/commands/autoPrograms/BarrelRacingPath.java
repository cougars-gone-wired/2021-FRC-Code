package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.DriveCircle;
import frc.robot.commands.DriveForward;
import frc.robot.commands.DriveSide;

public class BarrelRacingPath extends SequentialCommandGroup {
    // bot width = 0.457m
    // bot length = 0.8128m
    // 20in = 0.508m
    // 25in = 0.635m
    // 30in = 0.762m 
    // 80in = 2.032m
    // 90in = 2.286m
    // 92 = 2.3368
    // 94 = 2.3876
    // 95 = 2.413
    // 115 = 2.921
    // 120in = 3.048m
    // 270in = 6.858
    private double circleSpeed = 0.17;
    private double driveSpeed = 0.3;
    
    public BarrelRacingPath() {
        addCommands(
            new DriveSide(true),
            // new DriveForward(2.286 + 0.4064, driveSpeed),
            new DriveForward(2.3876, driveSpeed),
            new DriveCircle(0.635, circleSpeed, 132),
            new DriveForward(2.921, driveSpeed),
            new DriveCircle(0.635, circleSpeed, -100),
            new DriveForward(2.032, driveSpeed),
            new DriveCircle(0.635, circleSpeed, -85),
            new DriveForward(6.858, driveSpeed)
        );
    }
}
