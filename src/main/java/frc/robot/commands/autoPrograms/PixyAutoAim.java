package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.PixySeekBall;
import frc.robot.commands.PixySetTargetBall;
import frc.robot.commands.IntakeArmDown;

public class PixyAutoAim extends SequentialCommandGroup {

    public PixyAutoAim() {
        addCommands(
            new IntakeArmDown(),
            new PixySetTargetBall(),
            new PixySeekBall()
        );

    }
}