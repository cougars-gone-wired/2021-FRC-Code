package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import frc.robot.commands.ChomperDown;
import frc.robot.commands.ChomperUp;
import frc.robot.commands.DoNothing;

//Called Perpetually in AutoSelector!
public class ChomperTest extends ParallelCommandGroup {
    private int delay = 30;

    public ChomperTest() {
        addCommands(
            new DoNothing(delay),
            new ChomperUp(),
            new DoNothing(delay),
            new ChomperDown()
        );
    }
}
