package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.PixySeekBall;
import frc.robot.commands.PixySeekEnd;
import frc.robot.commands.PixyAquire;
// import frc.robot.commands.PixySetTargetBall;
// import frc.robot.commands.PixySetTargetEnd;
import frc.robot.commands.StopIntaking;
import frc.robot.commands.ArmDown;
import frc.robot.commands.ArmUp;
import frc.robot.commands.DoNothing;
import frc.robot.commands.DriveForward;
import frc.robot.commands.IntakeArmDown;
import frc.robot.commands.IntakeArmUp;
import frc.robot.commands.Intaking;

public class GalacticSearch extends SequentialCommandGroup {

    public GalacticSearch() {
        addCommands(
            new IntakeArmDown(),
            new ArmDown(),
            new DoNothing(10),

            // new PixySetTargetBall(),
            new PixySeekBall(),
            new Intaking(),
            new PixyAquire(),
            new DriveForward(0.3),
            // new StopIntaking(),

            // new PixySetTargetBall(),
            new PixySeekBall(),
            // new Intaking(),
            new PixyAquire(),
            new DriveForward(0.3),
            // new StopIntaking(),

            // new PixySetTargetBall(),
            new PixySeekBall(),
            // new Intaking(),
            new PixyAquire(),
            new DriveForward(0.3),
            new StopIntaking(),

            new ArmUp(),

            // new PixySetTargetEnd(),
            new PixySeekEnd(),
            new PixyAquire(),
            new IntakeArmUp()
        );
    }
}