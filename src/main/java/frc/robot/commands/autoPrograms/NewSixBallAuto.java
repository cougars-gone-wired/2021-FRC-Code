package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.Aim;
import frc.robot.commands.ArmDown;
import frc.robot.commands.ChomperUp;
import frc.robot.commands.DoNothing;
import frc.robot.commands.DriveBack;
import frc.robot.commands.DriveForward;
import frc.robot.commands.FeederIntake;
import frc.robot.commands.FeederNotMoving;
import frc.robot.commands.IntakeArmDown;
import frc.robot.commands.Intaking;
import frc.robot.commands.ShootVoltage;
import frc.robot.commands.StopIntaking;
import frc.robot.commands.Unaim;

public class NewSixBallAuto extends SequentialCommandGroup {

    public NewSixBallAuto() {
        addCommands(
            new ArmDown(),
            new IntakeArmDown(),
            new DriveForward(1.25),
            new DoNothing(10),
            new Aim().withTimeout(5),
            // new ShootPID(0.457).withTimeout(4.5),
            new ShootVoltage(0.430, false).withTimeout(5.3),
            new Unaim().withTimeout(5),
            new Intaking(),
            new DriveForward(3.5, 0.3),
            new StopIntaking(),
            new FeederIntake(),
            new ChomperUp(),
            new DoNothing(10),
            new DriveBack(2.2),
            new FeederNotMoving(),
            new DriveBack(0.6),
            new DoNothing(10),
            new Aim().withTimeout(5.0),
            // new ShootPID(0.457).withTimeout(5),
            new ShootVoltage(0.440, false).withTimeout(5.5),
            new ChomperUp(),
            new Unaim().withTimeout(5)
        );
    }
}