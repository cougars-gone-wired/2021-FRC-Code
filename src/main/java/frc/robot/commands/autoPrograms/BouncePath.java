package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.DriveCircle;
import frc.robot.commands.DriveForward;
import frc.robot.commands.DriveSide;

public class BouncePath extends SequentialCommandGroup {
    // bot width = 0.457m
    // bot length = 0.8128m
    // 10in = 0.254m
    // 15in = 0.381m
    // 20in = 0.508m
    // 22in = 0.5588
    // 25in = 0.635m
    // 30in = 0.762m 
    // 32 = 0.8128
    // 80 = 2.032
    // 90in = 2.286m
    // 95in = 2.413m
    private double circleSpeed = 0.13;
    private double driveSpeed = 0.25;
    
    public BouncePath() {
        addCommands(
            new DriveSide(true),
            new DriveCircle(0.762, circleSpeed, -33),
            new DriveForward(0.254, driveSpeed),
            new DriveSide(false),
            new DriveCircle(0.762, circleSpeed, -19),
            new DriveCircle(0.762, circleSpeed, 20),
            new DriveForward(0.381, driveSpeed),
            new DriveCircle(0.5588, circleSpeed, -56.7),
            new DriveForward(2.413, driveSpeed),
            new DriveSide(true),
            new DriveForward(2.032, driveSpeed),
            new DriveCircle(0.5588, circleSpeed, -28),
            new DriveForward(0.8128, driveSpeed),
            new DriveCircle(0.5588, circleSpeed, -28),
            new DriveForward(2.286, driveSpeed),
            new DriveSide(false),
            new DriveForward(0.381, driveSpeed),
            new DriveCircle(0.762, circleSpeed, -30)
        );
    }
}
