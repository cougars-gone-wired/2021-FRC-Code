package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.ArmDown;
import frc.robot.commands.IntakeArmDown;
import frc.robot.commands.Aim;
import frc.robot.commands.Unaim;
import frc.robot.commands.ShootVoltage;

public class AimShootAndUnaim extends SequentialCommandGroup {

    public AimShootAndUnaim() {
        addCommands(
            new ArmDown(),
            new IntakeArmDown().withTimeout(1),
            new Aim(),
            // new ShootPID(.457).withTimeout(5),
            new ShootVoltage(.450, false).withTimeout(5),
            new Unaim()
        );
    }
}