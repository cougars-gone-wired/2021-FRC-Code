package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Robot;
import frc.robot.commands.Aim;
import frc.robot.commands.ArmDown;
import frc.robot.commands.ProfileDrive;
import frc.robot.commands.ShootVoltage;

public class ShootFromTrench extends SequentialCommandGroup {

    public ShootFromTrench() {
        addCommands(
            new ArmDown(),
            new ProfileDrive(Robot.drive).getProfilingCommand("paths/output/TrenchShootingPose.wpilib.json"),
            new Aim(),
            new ShootVoltage(0.457, true).withTimeout(5)
        );
    }
}