package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.DriveCircle;
import frc.robot.commands.DriveForward;
import frc.robot.commands.DriveSide;

public class SlalomPath extends SequentialCommandGroup {
    // bot width = 0.457m
    // bot length = 0.8128m
    // 8in = 0.2032m
    // 10in = 0.254m
    // 15in = 0.381m
    // 20in - 0.508m
    // 24in = 0.6096
    // 25in = 0.635m
    // 27 =  0.6858
    // 30in = 0.762m 
    // 32in = 0.8128
    // 84in = 2.1336
    // 80 = 2.032
    // 88 = 2.2352
    // 90in = 2.286m
    // 92 = 2.3368
    private double circleSpeed = 0.2; //.2
    private double driveSpeed = 0.39; //.39

    public SlalomPath() {
        addCommands(
            new DriveSide(true),
            new DriveCircle(0.8128, circleSpeed, -25),
            new DriveCircle(0.635, circleSpeed, 33),
            new DriveForward(2.3368, driveSpeed),
            new DriveCircle(0.6858, circleSpeed, 35),
            new DriveCircle(0.6096, circleSpeed, -120),
            new DriveCircle(0.635, circleSpeed, 35),
            new DriveForward(2.032, driveSpeed),
            new DriveCircle(0.762, circleSpeed, 35),
            new DriveCircle(0.6858, circleSpeed, -35),
            new DriveForward(0.381, driveSpeed)
        );
    }
}