package frc.robot.commands.autoPrograms;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.DriveCircle;
import frc.robot.commands.DriveSide;

public class SimpleCircle extends SequentialCommandGroup {
    public SimpleCircle() {
        addCommands(
            new DriveSide(true),
            new DriveCircle(0.5714/2, 0.15, 100)
        );
    }
}
