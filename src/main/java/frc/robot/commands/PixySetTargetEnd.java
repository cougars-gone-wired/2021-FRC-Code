package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import io.github.pseudoresonance.pixy2api.Pixy2CCC;
import io.github.pseudoresonance.pixy2api.Pixy2CCC.Block;

public class PixySetTargetEnd extends CommandBase {
    Block targetBlock;
    
    // @Override
    public void initalize() {
        targetBlock = Robot.pixy.getLargestBlock(Pixy2CCC.CCC_SIG2);
        Robot.pixy.setTargetBlock(targetBlock);
    }

    @Override
    public boolean isFinished() {
        // return isAimed;
        return true;
    }
    
}
