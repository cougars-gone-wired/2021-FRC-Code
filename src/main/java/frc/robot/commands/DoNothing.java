package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;

public class DoNothing extends CommandBase {

    private int count;

    public DoNothing(int count) {
        this.count = count;
    }

    public void execute() {
        count--;
    }

    @Override
    public boolean isFinished() {
        return count <= 0;
    }
}