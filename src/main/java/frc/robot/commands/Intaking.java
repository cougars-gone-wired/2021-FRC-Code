package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class Intaking extends CommandBase {

    @Override
    public void initialize() {
        Robot.chomper.setIdle();
        Robot.intake.setIntaking();
        Robot.feeder.setIntaking();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}