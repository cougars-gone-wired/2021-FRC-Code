package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class FeederNotMoving extends CommandBase {

    @Override
    public void initialize() {
        Robot.feeder.setNotMoving();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}