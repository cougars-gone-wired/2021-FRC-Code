package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class DriveSide extends CommandBase{

    boolean setShooterSide;
    
    public DriveSide(boolean shooterSide) {
        setShooterSide = shooterSide;
    }

    public void execute() {
        if(setShooterSide) {
            Robot.drive.setShooterSide();
        } else {
            Robot.drive.setIntakeSide();
        }
        
    }

    public boolean isFinished() {
        return setShooterSide == Robot.drive.isShooterSide();
    }
}
