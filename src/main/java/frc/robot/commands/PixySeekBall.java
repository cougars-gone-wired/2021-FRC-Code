package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import io.github.pseudoresonance.pixy2api.Pixy2CCC;

public class PixySeekBall extends CommandBase {
    boolean isAimed;

    @Override
    public void execute() {
        // targetBlock = Robot.pixy.getLargestBlock(Pixy2CCC.CCC_SIG1);
        // Robot.pixy.setTargetBlock(targetBlock);
        isAimed = Robot.pixy.seek(Pixy2CCC.CCC_SIG1);
    }

    @Override
    public boolean isFinished() {
        return isAimed;
        // return false;
    }
}