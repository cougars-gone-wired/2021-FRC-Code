package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class ArmUp extends CommandBase {

    @Override
    public void initialize() {
        Robot.arms.setStartingPosition();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}