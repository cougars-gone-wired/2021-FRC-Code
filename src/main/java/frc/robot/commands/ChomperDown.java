package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class ChomperDown extends CommandBase {

    @Override
    public void initialize() {
        Robot.chomper.setOverride();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}