package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.Constants.DriveConstants;

public class DriveCircle extends CommandBase {

    private double circumference;

    private double leftSpeed;
    private double rightSpeed;

    private double h;
    private boolean clockwise;

    //radius in meters
    public DriveCircle(double radius, double speed, double percentage) {
        Robot.drive.getEncoders().resetEncoders();

        h = DriveConstants.TRACK_WIDTH / 2.0;
        clockwise = percentage >= 0;

        circumference = 2.0 * Math.PI * (radius + h) * (Math.abs(percentage) / 100);

        if(clockwise) {
            leftSpeed = speed;
            rightSpeed = speed * DriveConstants.TURN_FACTOR * ((radius - h) / (radius + h)); 
        } else {
            rightSpeed = speed;
            leftSpeed = speed * DriveConstants.TURN_FACTOR * ((radius - h) / (radius + h));
        
        }
    }

    @Override
    public void execute() {
        Robot.drive.driveLeft(leftSpeed);
        Robot.drive.driveRight(rightSpeed);
        SmartDashboard.putBoolean("ShooterSide", Robot.drive.isShooterSide());
        SmartDashboard.putBoolean("FinCircle", false);
        SmartDashboard.putNumber("AvgLeftEncoders", Robot.drive.getEncoders().getLeftEncodersMeters());
        SmartDashboard.putNumber("AvgRightEncoders", Robot.drive.getEncoders().getRightEncodersMeters());
        SmartDashboard.putNumber("DesiredCircumference", circumference);
    }

    @Override
    public boolean isFinished() {
        if((clockwise && Robot.drive.isShooterSide()) || (!clockwise && Robot.drive.isIntakeSide())) {
            if(Math.abs(Robot.drive.getEncoders().getLeftEncodersMeters()) >= circumference) {
                finish();
                return true;
            }
        } else {
            if(Math.abs(Robot.drive.getEncoders().getRightEncodersMeters()) >= circumference) {
                finish();
                return true;
            }
        }
        return false;
    }

    public void finish() {
        SmartDashboard.putBoolean("FinCircle", true);
        Robot.drive.tankDriveVolts(0, 0);
        Robot.drive.getEncoders().resetEncoders();
    }
}