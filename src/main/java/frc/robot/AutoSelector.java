package frc.robot;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.*;
import frc.robot.commands.autoPrograms.*;

public class AutoSelector {

    private SendableChooser<Programs> autoChooser = new SendableChooser<>();

    private Command autoCommand;

    public enum Programs {
        DO_NOTHING, AIM, AIM_AND_UNAIM, AIM_SHOOT_AND_UNAIM, SHOOT_FROM_LINE_FRONT_BUMPER, SHOOT_FROM_LINE_BACK_BUMPER, OFFLINE_AND_SHOOT, SHOOT_FROM_TRENCH, SIX_BALL_AUTO, NEW_SIX_BALL_AUTO,DRIVE_RECKONING, DRIVE_PROFILING, SIMPLE_CIRCLE, BARREL_RACING_PATH, SLALOM_PATH, BOUNCE_PATH, PIXY_AUTO_AIM, GALACTIC_SEARCH, PISTON_TEST, CHOMPER_TEST
    }

    private Programs autoChoice;

    public AutoSelector() {
        initialize();
    }

    public void initialize() {
        autoChooser.addOption("Do Nothing", Programs.DO_NOTHING);

        autoChooser.addOption("Aim", Programs.AIM);
        autoChooser.addOption("Aim and Unaim", Programs.AIM_AND_UNAIM);
        autoChooser.addOption("Aim Shoot and Unaim", Programs.AIM_SHOOT_AND_UNAIM);
        autoChooser.addOption("Shoot From Line", Programs.SHOOT_FROM_LINE_BACK_BUMPER);
        autoChooser.addOption("Offline and Shoot", Programs.OFFLINE_AND_SHOOT);
        autoChooser.addOption("Shoot From Trench", Programs.SHOOT_FROM_TRENCH);
        autoChooser.addOption("Six Ball Auto", Programs.SIX_BALL_AUTO);
        autoChooser.addOption("New Six Ball Auto", Programs.NEW_SIX_BALL_AUTO);
        autoChooser.addOption("Drive Reckoning", Programs.DRIVE_RECKONING);
        autoChooser.addOption("Drive Profiling", Programs.DRIVE_PROFILING);
        
        autoChooser.addOption("Simple Circle", Programs.SIMPLE_CIRCLE);
        autoChooser.addOption("Barrel Racing", Programs.BARREL_RACING_PATH);
        autoChooser.addOption("Slalom", Programs.SLALOM_PATH);
        autoChooser.addOption("Bounce", Programs.BOUNCE_PATH);
        
        autoChooser.addOption("Pixy Auto Aim", Programs.PIXY_AUTO_AIM);
        autoChooser.addOption("Galactic Search", Programs.GALACTIC_SEARCH);

        autoChooser.addOption("Piston Test", Programs.PISTON_TEST);
        autoChooser.addOption("Chomper Test", Programs.CHOMPER_TEST);
        SmartDashboard.putData("Auto", autoChooser);
    }

    public Command getAutoCommand() {
        autoChoice = autoChooser.getSelected();

        SmartDashboard.putString("AutoChoice", autoChoice.toString());
        switch (autoChoice) {
            case DO_NOTHING:
                autoCommand = new DoNothing(20);
                break;

            case AIM:
                autoCommand = new SequentialCommandGroup(new ArmDown().withTimeout(5), new Aim());
                break;

            case AIM_AND_UNAIM:
                autoCommand = new SequentialCommandGroup(new ArmDown().withTimeout(5), new Aim().withTimeout(5), new Unaim());
                break;

            case AIM_SHOOT_AND_UNAIM:
                autoCommand = new AimShootAndUnaim();
                break;

            case SHOOT_FROM_LINE_FRONT_BUMPER:
                autoCommand = new ShootFromLineFrontBumper();
                break;

            case SHOOT_FROM_LINE_BACK_BUMPER:
                autoCommand = new ShootFromLineBackBumper();
                break;

            case OFFLINE_AND_SHOOT:
                autoCommand = new OfflineAndShoot();
                break;

            case SHOOT_FROM_TRENCH:
                autoCommand = new ShootFromTrench();
                break;

            case SIX_BALL_AUTO:
                autoCommand = new SixBallAuto();
                break;
            
            case NEW_SIX_BALL_AUTO:
                autoCommand = new NewSixBallAuto();
                break;

            case DRIVE_RECKONING:
                autoCommand = new DriveBack(1);
                // autoCommand = new DriveForward(1);
                break;

            case DRIVE_PROFILING:
                autoCommand = new ProfileDrive(Robot.drive).getProfilingCommand("paths/output/TrenchShootingPose.wpilib.json");
                break;

            case SIMPLE_CIRCLE:
                autoCommand = new SimpleCircle();
                break;

            case BARREL_RACING_PATH:
                autoCommand = new BarrelRacingPath();
                break;

            case SLALOM_PATH:
                autoCommand = new SlalomPath();
                break;

            case BOUNCE_PATH:
                autoCommand = new BouncePath();
                break;
            
            case PIXY_AUTO_AIM:
                autoCommand = new PixyAutoAim();
                break;

            case GALACTIC_SEARCH:
                autoCommand = new GalacticSearch();
                break;

            case PISTON_TEST:
                autoCommand = new PistonTest().perpetually();
                break;

            case CHOMPER_TEST:
                autoCommand = new ChomperTest().perpetually();
                break;
                
            default:
                autoCommand = new DoNothing(0);
                break;
        }
        return autoCommand;
    }
}