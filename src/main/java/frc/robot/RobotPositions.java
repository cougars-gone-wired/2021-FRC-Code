package frc.robot;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class RobotPositions {

    public static double DEFAULT_GREEN_SHOOTING_SPEED = 0.265;
    public static double DEFAULT_YELLOW_SHOOTING_SPEED = 0.47;
    public static double DEFAULT_BLUE_SHOOTING_SPEED = 0.425;
    public static double DEFAULT_RED_SHOOTING_SPEED = 0.47;

    private double greenShootingSpeed;
    private double yellowShootingSpeed;
    private double blueShootingSpeed;
    private double redShootingSpeed;

    public RobotPositions() {
        greenShootingSpeed = DEFAULT_GREEN_SHOOTING_SPEED;
        yellowShootingSpeed = DEFAULT_YELLOW_SHOOTING_SPEED;
        blueShootingSpeed = DEFAULT_BLUE_SHOOTING_SPEED;
        redShootingSpeed = DEFAULT_RED_SHOOTING_SPEED;
    }

    public void initialize() {
        currentPositon = Positions.DEFAULT;
        greenShootingSpeed = DEFAULT_GREEN_SHOOTING_SPEED;
        yellowShootingSpeed = DEFAULT_YELLOW_SHOOTING_SPEED;
        blueShootingSpeed = DEFAULT_BLUE_SHOOTING_SPEED;
        redShootingSpeed = DEFAULT_RED_SHOOTING_SPEED;
    }

    public void initPositionDashboard() {
        SmartDashboard.putString("Current Position", currentPositon.toString());
        SmartDashboard.putNumber("greenShootingSpeed", greenShootingSpeed);
        SmartDashboard.putNumber("yellowShootingSpeed", yellowShootingSpeed);
        SmartDashboard.putNumber("blueShootingSpeed", blueShootingSpeed);
        SmartDashboard.putNumber("redShootingSpeed", redShootingSpeed);
        SmartDashboard.putBoolean("Set Speeds", false);
    }

    public void positionDashboard() {
        SmartDashboard.putString("Current Position", currentPositon.toString());
        if(SmartDashboard.getBoolean("Set Speeds", false)) {
            greenShootingSpeed = SmartDashboard.getNumber("greenShootingSpeed", DEFAULT_GREEN_SHOOTING_SPEED);
            yellowShootingSpeed = SmartDashboard.getNumber("yellowShootingSpeed", DEFAULT_YELLOW_SHOOTING_SPEED);
            blueShootingSpeed = SmartDashboard.getNumber("blueShootingSpeed", DEFAULT_BLUE_SHOOTING_SPEED);
            redShootingSpeed = SmartDashboard.getNumber("redShootingSpeed", DEFAULT_RED_SHOOTING_SPEED);
            SmartDashboard.putBoolean("Set Speeds", false);
        }
    }

    public enum Positions{
        GREEN_POSITION, YELLOW_POSITION, BLUE_POSITION, RED_POSITION, DEFAULT
    }

    private Positions currentPositon;

    public void controlPositons(boolean greenPositonButton, boolean yellowPositionButton, boolean bluePositionButton, boolean redPositionButton) {
        if(greenPositonButton) {
            setGreenPosition();
        } else if(yellowPositionButton) {
            setYellowPosition();
        } else if(bluePositionButton) {
            setBluePosition();
        } else if(redPositionButton) {
            setRedPosition();
        }
    }

    public boolean isGreenPosition() { 
        return currentPositon == Positions.GREEN_POSITION;
    }

    public boolean isYellowPosition() { 
        return currentPositon == Positions.YELLOW_POSITION;
    }

    public boolean isBluePosition() { 
        return currentPositon == Positions.BLUE_POSITION;
    }

    public boolean isRedPosition() { 
        return currentPositon == Positions.RED_POSITION;
    }

    public boolean isDefault() {
        return currentPositon == Positions.DEFAULT;
    }

    public void setGreenPosition() {
        Robot.arms.setStartingPosition();
        Robot.shooter.setShooterSpeed(greenShootingSpeed);
        currentPositon = Positions.GREEN_POSITION;
    }

    public void setYellowPosition() {
        Robot.arms.setShootingPosition();
        Robot.shooter.setShooterSpeed(yellowShootingSpeed);
        currentPositon = Positions.YELLOW_POSITION;
    }

    public void setBluePosition() {
        Robot.arms.setShootingPosition();
        Robot.shooter.setShooterSpeed(blueShootingSpeed);
        currentPositon = Positions.BLUE_POSITION;
    }

    public void setRedPosition() {
        Robot.arms.setShootingPosition();
        Robot.shooter.setShooterSpeed(redShootingSpeed);
        currentPositon = Positions.RED_POSITION;
    }

    public void setDefault() {
        Robot.shooter.setShooterSpeed(Robot.shooter.getInitialShooterSpeed());
        currentPositon = Positions.DEFAULT;
    }

}
