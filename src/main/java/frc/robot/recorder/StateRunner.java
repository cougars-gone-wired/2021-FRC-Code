package frc.robot.recorder;

import java.util.List;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot; 

public class StateRunner {

    List<State> states;

    int counter = 0;

    public StateRunner() {
    }

    public void initialize() {
        counterInitialize();

        try {
            setStates(StateReader.read(StateLister.getGsonChooser().getSelected()));
        } catch (Exception e) {
            
        }
    }

    public void counterInitialize() {
        counter = 0;
    }

    public void setStates(List<State> states) {
        this.states = states;
    }

    public void run() {
        if (counter < states.size()) {
            State s = states.get(counter);

            Robot.drive.robotDrive(s.getDriveSpeedAxisState(), s.getDriveTurnAxisState(), s.getSwitchSideState());

            counter++;
            SmartDashboard.putNumber("Counter", counter);
        }
    }
}
